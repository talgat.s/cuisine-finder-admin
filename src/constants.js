export const MAIN =
	process.env.NODE_ENV === "development"
		? "http://localhost:4040/"
		: "https://cuisine-finder-js.herokuapp.com/";
